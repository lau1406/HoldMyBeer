# Copyright 2013 Thatcher Peskens
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM alpine:latest

MAINTAINER Dockerfiles

# Install required packages and remove the apt packages cache when done.
#apk update && apk upgrade && \
RUN apk update 
RUN apk add bash \
	git \
	openssh \
	python3 \
	python3-dev \
	gcc \
	build-base \
	linux-headers \
	pcre-dev \
	postgresql-dev \
	musl-dev \
	libxml2-dev \
	jpeg-dev \
	libxslt-dev \
	nginx \
	curl \
    libffi-dev \
	supervisor && \
	python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    rm -r /root/.cache && \
    pip3 install --upgrade pip setuptools && \
    rm -r /root/.cache

# install uwsgi now because it takes a little while
RUN pip3 install uwsgi

# setup all the configfiles
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/nginx-app.conf /etc/nginx/sites-available/default
COPY docker/supervisor-app.conf /etc/supervisor/conf.d/

# COPY requirements.txt and RUN pip install BEFORE adding the rest of your code, this will cause Docker's caching mechanism
# to prevent re-installing (all your) dependencies when you made a change a line or two in your app.
COPY requirements.txt /home/docker/code/
RUN pip3 install -r /home/docker/code/requirements.txt

# add (the rest of) our code
COPY CountMyBeer/ /home/docker/code/CountMyBeer/
COPY HoldMyBeer/ /home/docker/code/HoldMyBeer/
COPY manage.py /home/docker/code/
COPY docker/start_server.sh /home/docker/code/
COPY docker/uwsgi.ini /home/docker/code/
COPY docker/uwsgi_params /home/docker/code/
COPY .env.example /home/docker/code/

RUN mkdir /data
RUN mkdir -p /home/docker/code/data
VOLUME /data
VOLUME /home/docker/code/data/


# Set env variables
ENV DATABASE=/data/db.sqlite3
ENV STATIC=/data/static
ENV MEDIA=/data/media
WORKDIR /home/docker/code/

EXPOSE 8113
CMD ["/bin/sh", "/home/docker/code/start_server.sh"]
#HEALTHCHECK CMD curl --fail http://localhost:8113/
