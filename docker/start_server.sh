#!/bin/bash
# Copy .env.example file (somehow doesn't work during build)
cp /home/docker/code/.env.example /data

# Collect static files
echo "Collect static files"
python3 /home/docker/code/manage.py collectstatic --noinput

# Apply database migrations
echo "Apply database migrations"
python3 /home/docker/code/manage.py migrate --noinput

# Create superuser if not already exists
echo "Creating superuser if not already exists"
echo "from django.contrib.auth import get_user_model; get_user_model().objects.filter(username='$SUPERUSER').exists() or get_user_model().objects.create_superuser('$SUPERUSER', '$SUPERUSER_EMAIL', '$SUPERUSER_PASSWORD')" | python3 /home/docker/code/manage.py shell

# Start server
supervisord -n -c /etc/supervisor/conf.d/supervisor-app.conf
