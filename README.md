# Hold My Beer 
Hold My Beer is a website where you and your guests can keep track of what everyone consumed. In the back-end you can configure what drinks or food you have and guests can login and select what they consume and see the total cost.

## Setup
### Non Docker
1. Copy `.env.example` and name it `.env`
2. Edit all relevant variables in `.env`
3. Create a python virtual environment
4. Install all packages from `requirements.txt` in this environment
5. Run `python manage.py migrate`
6. Run `python manage.py createsuperuser`

### Docker
Specify the following docker paths:
```
/data
```
Specify the following variables:
```
STATIC (the static file location)
MEDIA (the media file location)
SUPERUSER (superuser username)
SUPERUSER_EMAIL (superuser email)
SUPERUSER_PASSWORD (superuser password)
```
Specify the following ports:
```
8113 (http port)
```
