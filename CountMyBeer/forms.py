from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from CountMyBeer.models import User


class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'is_borrel')


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'is_borrel')
