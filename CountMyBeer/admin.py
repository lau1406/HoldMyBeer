from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin, highlight_deleted

from CountMyBeer.models import *
from django.contrib.auth.admin import UserAdmin
from CountMyBeer.models import User

from django.utils.translation import ugettext_lazy as _
from CountMyBeer.forms import CustomUserCreationForm, CustomUserChangeForm


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_editable = ()
    list_display = ('id', 'username', 'is_borrel')
    list_display_links = ('id', 'username')
    list_filter = ('is_borrel',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'is_borrel', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'is_borrel'),
        }),
    )


@admin.register(Drink)
class DrinkAdmin(SafeDeleteAdmin):
    search_fields = ('name', 'id', 'title')
    list_display = ('id', highlight_deleted, 'price', 'volume_ml') + SafeDeleteAdmin.list_display
    list_display_links = ('id', highlight_deleted)
    list_editable = ('price', 'volume_ml')
    list_filter = ()


@admin.register(Food)
class FoodAdmin(SafeDeleteAdmin):
    search_fields = ('name', 'id', 'title')
    list_display = ('id', highlight_deleted, 'price', 'weight_gram') + SafeDeleteAdmin.list_display
    list_display_links = ('id', highlight_deleted)
    list_editable = ('price', 'weight_gram')
    list_filter = ()


@admin.register(UserHasProduct)
class UserHasProductAdmin(SafeDeleteAdmin):
    search_fields = ('user', 'product', 'amount')
    list_display = ('id', highlight_deleted, 'user', 'product', 'amount') + SafeDeleteAdmin.list_display
    list_display_links = ('id', highlight_deleted)
    list_editable = ('amount', )
    list_filter = ('user', 'product')


@admin.register(Payment)
class PaymentAdmin(SafeDeleteAdmin):
    search_fields = ('name', 'id', 'title')
    list_display = ('id', highlight_deleted, 'amount', 'created_at', 'updated_at', 'user') + SafeDeleteAdmin.list_display
    list_display_links = ('id', highlight_deleted)
    list_editable = ('amount', )
    list_filter = ('user', )
