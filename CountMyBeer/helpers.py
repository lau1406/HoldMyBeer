"""
Little helper file with useful small functions
"""
from CountMyBeer.models import Product, UserHasProduct, User


def generate_user_has_products(user: User) -> None:

    # Generate all UserHasProducts
    products = Product.objects.all()

    for product in products:
        # TODO: Fix bodge, problem with safedelete and polymorphic managers both wanting to change the objects function
        if product.deleted:
            continue
        UserHasProduct.objects.get_or_create(user=user, product=product)
