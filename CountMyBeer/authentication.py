from rest_framework.permissions import BasePermission


class IsBorrelUser(BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_borrel)


class IsBorrelUserOrAdmin(BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return bool(request.user and (request.user.is_borrel or request.user.is_staff))
