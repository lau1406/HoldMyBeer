from django import template
from django.db.models import Sum, F, FloatField
from django.contrib.auth import get_user_model
from CountMyBeer.models import UserHasProduct, Product
from CountMyBeer.helpers import generate_user_has_products
User = get_user_model()

register = template.Library()


@register.simple_tag
def total_cost(user_id):
    if user_id is None:
        return ''
    # Generate all user_has_products
    # Get the user
    user = User.objects.filter(id=user_id)
    if user.count() == 0:
        return ''
    user = user.first()

    # Generate all userhasproducts
    generate_user_has_products(user=user)

    price = UserHasProduct.objects.filter(user__id=user_id).aggregate(total=Sum(F('amount') * F('product__price'), output_field=FloatField()))['total']
    if price is None:
        return ''
    # Deduct all relevant payments
    payments = user.payment_set.all().aggregate(total=Sum('amount', output_field=FloatField()))['total']

    if payments is not None:
        price -= payments

    return '€{:,.2f}'.format(price)
