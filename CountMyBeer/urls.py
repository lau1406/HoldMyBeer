from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework import routers
from CountMyBeer import views

router = routers.DefaultRouter()
router.register('drink', views.DrinkViewSet)
router.register('food', views.FoodViewSet)
router.register('product', views.ProductViewSet)
router.register('payment', views.PaymentViewSet)
router.register('create_payment', views.CreatePaymentViewSet)
router.register('all_payments', views.AllPaymentsViewSet)
router.register('user_product', views.UserProductViewSet)
router.register('borrel_user_product', views.BorrelUserProductViewSet)
router.register('update_amount', views.UpdateAmount)
router.register('update_amount_borrel', views.UpdateAmountBorrel)
router.register('overview_user_product', views.OverviewUserProductViewSet)
router.register('user', views.UserViewSet)

urlpatterns = [
    path('', TemplateView.as_view(template_name='CountMyBeer/index.html'), name='index'),
    path('overview/', TemplateView.as_view(template_name='CountMyBeer/overview.html'), name='overview'),
    path('borrel/', TemplateView.as_view(template_name='CountMyBeer/borrel.html'), name='borrel'),
    path('api/v1/', include(router.urls)),
]
