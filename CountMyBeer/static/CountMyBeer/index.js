'use strict';
// Global vars
let drinks = null;
let foods = null;
let payments = [];
let products = {};
let id_name_map = new Map();


$(document).ready(function () {
    nunjucks.configure(staticPath, {
        web: {
            useCache: true
        }
    });

    setup_ajax();
    get_data();
});


function get_data() {
    get_products()
        .then(get_payments)
        .then(populate_initial)
}


function setup_ajax() {
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
            }
        }
    });
}


function populate_initial() {
    sort_products();
    let product_row = $('#product_row');

    for (const [key, value] of id_name_map.entries()) {
        const prod = products[key].product;
        product_row.append(create_card(prod.image, prod.name, prod.description, prod.price, products[key].amount, products[key].id, 'increment_amount(' + products[key].id + ')'));
    }
}


function sort_products() {
    // Create temporary array with the mappings
    let a = [];
    for (let k in products) {
        a.push([k, products[k].product.name]);
    }

    // Sort the array on the name of the product
    a.sort((a, b) => a[1].localeCompare(b[1]));

    // Finally create the new sorted map
    id_name_map = new Map(a);
}


function create_card(image, title, text, price, amount, id, onclick) {
    let njkRender = nunjucks.render('consumption-card.njk', {image, title, text, price, amount, id, onclick});

    return njkRender;
}


function update_product(data) {
    const id = data.id;
    // Update the amount for the correct product
    // Assumes the data exists, which should be considering data is the response from the increment_amount api call
    products[id].amount = data.amount;

    // Update the card
    $('#total_consumed_' + id).text('Total Consumed: ' + products[id].amount);
    update_total_cost();
}


function update_total_cost(extra) {
    let cost = extra ? parseFloat(extra) : 0;
    for (const [key, value] of id_name_map.entries()) {
        const prod = products[key].product;
        cost += products[key].amount * prod.price;
    }

    for (let i = 0; i < payments.length; i++) {
        cost -= payments[i].amount
    }

    $('#total_cost').text('Total Cost: €' + cost.toFixed(2));
}


function increment_amount(id) {
    // Fake increment gui for ease of use
    $('#total_consumed_' + id).text('Total Consumed: ' + (products[id].amount + 1));
    update_total_cost(products[id].product.price);

    patch_increment_amount(id, update_product);
}


function process_products(data) {
    // First clear products, just in case
    products = {};
    // Add products indexed by their id, id is unique
    for (let i = 0; i < data.length; i++) {
        products[data[i].id] = data[i];
    }
}


function patch_increment_amount(id, callback) {
    $.ajax({
        type: 'PATCH',
        url: '/api/v1/update_amount/' + id + '/',
        success: function (data) {
            callback(data);
        },
        fail: function (data) {
            console.log('failed incrementing amount');
        }
    })
}


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


function get_drinks() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/drink',
        success: function (data) {
            drinks = data;
        },
        fail: function () {
            console.log('failed getting drink data');
        }
    })
}

function get_food() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/food',
        success: function (data) {
            foods = data;
        },
        fail: function () {
            console.log('failed getting food data');
        }
    })
}

function get_payments() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/payment',
        success: function (data) {
            payments = data;
        },
        fail: function () {
            console.log('failed getting payment data');
        }
    })
}

function get_products() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/user_product',
        success: function (data) {
            process_products(data);
        },
        fail: function () {
            console.log('failed getting user products');
        }
    })
}

