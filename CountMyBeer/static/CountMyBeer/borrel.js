'use strict';
// Global vars
let nj;

let products = {};
let user_product_ids = {};
let users = [];
let id_name_map = new Map();


$(document).ready(function () {
    nj = nunjucks.configure(staticPath, {
        web: {
            useCache: true
        }
    });
    nj.addFilter('nameValue', name => name.split('').reduce((acc, cur) => acc + cur.toLowerCase().charCodeAt(0) - 97, 0));
    nj.addFilter('modulo', (val, base) => val % base);

    setup_ajax();
    get_data();
});


function get_data() {
    get_products()
        .then(populate_initial)

    get_users();
    get_user_products();
}


function setup_ajax() {
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
            }
        }
    });
}


function populate_initial() {
    sort_products();
    let product_row = $('#product_row');

    for (const [key, value] of id_name_map.entries()) {
        const prod = products[key];

        product_row.append(create_card(prod.image, prod.name, prod.description, prod.price, prod.amount, prod.id, 'open_user_select(' + prod.id + ')'));
    }
}


function sort_products() {
    let a = Object.keys(products).map(key => [key, products[key].name]);

    // Sort the array on the name of the product
    a.sort((a, b) => a[1].localeCompare(b[1]));

    // Finally create the new sorted map
    id_name_map = new Map(a);
}


function create_card(image, title, text, price, amount, id, onclick) {
    let njkRender = nunjucks.render('consumption-card.njk', {image, title, text, price, amount, id, onclick, showTotal: false});

    return njkRender;
}


function open_user_select(id) {
    let userSelectPanel = create_user_select(users, id);
    $("body").append(userSelectPanel);
    
    get_users();
    get_user_products();
}


function close_selection() {
    $("#user-select-panel").remove();
}


function create_user_select(usersList, productId) {
    let njkRender = nunjucks.render('user-select.njk', {usersList, productId});

    return njkRender;
}


function process_selection() {
    let productId = $("#user-select-panel").attr("data-product-id");
    let userIds = $("#user-select-panel input[name=users]:checked").toArray().map(el => el.value);

    userIds.forEach(user => patch_increment_amount(user, productId, data => console.log(`increased product ${data.product.id} for user ${data.user}`)));

    close_selection();
}


function process_products(data) {
    // First clear products, just in case
    products = {};
    // Add products indexed by their id, id is unique
    for (let i = 0; i < data.length; i++) {
        products[data[i].id] = data[i];
    }
}


function process_user_product_mapping(data) {
    data.forEach(item => {
        if (!(item.user in user_product_ids)) {
            user_product_ids[item.user] = {};
        }

        user_product_ids[item.user][item.product.id] = item.id;
    });
}


function patch_increment_amount(userId, productId, callback = console.log) {
    if (!(userId in user_product_ids)) {throw new Error(`userId ${userId} does not exist in user_has_product table`)}
    if (!(productId in user_product_ids[userId])) {throw new Error(`productId ${productId} does not exist in user_has_product table`)}

    let user_product = user_product_ids[userId][productId];

    $.ajax({
        type: 'PATCH',
        url: `/api/v1/update_amount_borrel/${user_product}/`,
        success: function (data) {
            callback(data);
        },
        fail: function (data) {
            console.log('failed incrementing amount');
        }
    })
}


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


function get_users() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/user',
        success: function (data) {
            users = data;
        },
        fail: function () {
            console.log('failed getting user data');
        }
    })
}


function get_products() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/product',
        success: function (data) {
            process_products(data);
        },
        fail: function () {
            console.log('failed getting user products');
        }
    })
}


function get_user_products() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/borrel_user_product',
        success: function (data) {
            process_user_product_mapping(data);
        },
        fail: function () {
            console.log('failed getting user products');
        }
    })
}
