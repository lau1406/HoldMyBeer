from django.contrib.auth.base_user import BaseUserManager
from django.core.validators import MinValueValidator
from django.db import models
from django.contrib.auth.models import AbstractUser
from polymorphic.models import PolymorphicModel
from django.db.models.signals import post_save
from django.dispatch import receiver
from safedelete import SOFT_DELETE_CASCADE, SOFT_DELETE
from safedelete.models import SafeDeleteModel

# Create your models here.
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    """
    Custom user model, mainly for expanding later, but implemented at the beginning. Costs barely any effort in the
    beginning, but a lot of effort after the first migration

    :cvar roles: The roles a user can have
    """
    class Meta:
        verbose_name_plural = _('Users')
        verbose_name = _('User')

    is_borrel = models.BooleanField(default=False, help_text=_('True if someone is a borrel user'))

    def __str__(self):
        return self.username


class Payment(SafeDeleteModel):
    amount = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0)])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')

    _safedelete_policy = SOFT_DELETE_CASCADE

    def __str__(self):
        return str(self.amount)


class Profile(SafeDeleteModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    _safedelete_policy = SOFT_DELETE_CASCADE

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    Profile.objects.get_or_create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Product(PolymorphicModel, SafeDeleteModel):
    """
    Simple abstract class that forms the basis for liquid and solid food items
    """

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    _safedelete_policy = SOFT_DELETE_CASCADE

    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    image = models.ImageField(upload_to='products/')
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.name


class Drink(Product):
    """
    Simple model that represents a drink.
    Added the volume of the drink.
    Extends from AbstractFood.
    """
    volume_ml = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = _('Drink')
        verbose_name_plural = _('Drinks')


class Food(Product):
    """
    Simple model that represents solid food.
    Added the weight of the food.
    Extends from AbstractFood.
    """
    weight_gram = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = _('Food')
        verbose_name_plural = _('Foods')


class UserHasProduct(SafeDeleteModel):
    """
    Simple model to track which user had how much from what product.
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.user.username + ' ' + self.product.name

    class Meta:
        unique_together = ('user', 'product')
        verbose_name = _('User has Product')
        verbose_name_plural = _('User has Products')
