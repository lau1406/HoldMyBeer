from CountMyBeer.helpers import generate_user_has_products
from CountMyBeer.models import UserHasProduct, Payment
from django.contrib.auth import get_user_model
User = get_user_model()


class UserHasProductQuerySet(object):
    """
    TODO: Add docs
    """
    def get_queryset(self):
        # TODO: This way of creating all the objects creates more sql queries than needed, should probably be optimized
        user = self.request.user
        # In case this is the first time the user requests to see their status or a new product has been added,
        # create all the missing models for this user and the drinks/food
        generate_user_has_products(user=user)

        return UserHasProduct.objects.filter(user=user)


class UserHasProductBorrelQuerySet(object):
    """
    TODO: Add docs
    """
    def get_queryset(self):
        # TODO: This way of creating all the objects creates more sql queries than needed, should probably be optimized
        user = self.request.user
        # In case this is the first time the user requests to see their status or a new product has been added,
        # create all the missing models for this user and the drinks/food
        users = User.objects.all()
        for user in users:
            generate_user_has_products(user=user)

        return UserHasProduct.objects.all()


class PaymentQuerySet(object):
    """
    TODO: Add docs
    """
    def get_queryset(self):
        return Payment.objects.filter(user=self.request.user)
